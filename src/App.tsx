import React from 'react';
import './App.css';
import Header from "./components/Header";

function App() {
  return (
    <div className="App">
      <section className="mx-auto">
        <Header/>
      </section>
    </div>
  );
}

export default App;

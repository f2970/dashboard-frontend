default: dev

dev:
	docker build -t dashboard-frontend:dev .
	docker run -it --rm -p 3000:3000 dashboard-frontend:dev

prod:
	docker build -f Dockerfile.prod -t dashboard-frontend:prod .
	docker run -it --rm -p 3000:3000 dashboard-frontend:prod
